"""Script to generate one time pads"""

'''
otpGen
Copyright (C) 2018 Samuel First

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse
from quantumrandom import randint as qrandom
from random import randint as prandom
from random import SystemRandom as urandom

# Define Arguments
#  positional:
#   file: name of file to save to
#  flags:
#   -q: use quantum random number generator
#   -p: use pseudo random number generator
#   -u: use system random number generator
#   -s: size of pad to generate (chars/bytes)
#   -c: character set to use
#   -n: print to standard i/o instead of saving file
p = argparse.ArgumentParser(
    description="Script to generate one time pads",

    epilog="*Use of the pseudo flag is not recommended,\
    as it creates an insecure pad")

p.add_argument("file",
    type=str,
    nargs="?")

p.add_argument("-q",
    "--quantum",
    action="store_true",
    help="Use quantum random number generator")

p.add_argument("-p",
    "--pseudo",
    action="store_true",
    help="Use pseudo-random number generator*")

p.add_argument("-u",
    "--urandom",
    action="store_true",
    help="Use SystemRandom random number generator")

p.add_argument("-s",
    "--size",
    type=int, default=1024,
    help="Size of pad in bytes")

p.add_argument("-c",
    "--charset",
    type=str,
    default="abcdefghijklmnopqrstuvwxyz",
    help="Charset to use when generating pad")

p.add_argument("-n",
    "--nosave",
    action="store_true",
    help="Print one time pad instead of saving it")

args = p.parse_args()


otp = ""

# The python 3 version of quantumrandom.randint returns a float
# This function works around that by converting it to an int
def quantumCompat():
    """Fixes python3 version of quantumrandom.randint"""
    global otp
    r = int(qrandom(0, len(args.charset)))
    otp += args.charset[r]

# Writes the pad out as file
#  (the file name is passed through args.file)
def genPad(pad):
    """Writes pad out to file"""
    if not args.nosave:
        with open(args.file, 'w') as file:
            file.write(pad)
    else:
        print(pad)


# Generate pads with quantumrandom
# If quantumrandom is returning a float:
#  Set useCompat to True && generate using
#   the quantumCompat method
# Otherwise generate normally
def useQuantum():
    """Generates pad w/ quantum random rng"""
    global otp

    useCompat = False
    
    for i in range(args.size):
        if not useCompat:
            try:
                r = qrandom(0, len(args.charset) - 1)
                otp += args.charset[r]
            except TypeError:
                useCompat = True
                quantumCompat()
        else:
            quantumCompat()

# Creates System Random object && uses it to generate pad
def useSystem():
    """Generates pad w/ System Random rng"""
    global otp

    x = urandom()
                
    for i in range(args.size):
        r = x.randint(0, len(args.charset) - 1)
        otp += args.charset[r]

# Uses pseudo random number generator to generate pad
# Note: because the pseudo random number generator is
#       deterministic, this method creates insecure pads
def usePseudo():
    """Generates pad w/ pseudo random rng"""
    global otp

    for i in range(args.size):
        r = prandom(0, len(args.charset) - 1)
        otp += args.charset[r]

# Only run the script if it is being called from the terminal
#  Then check args && generate pad w/ chosen rng
if __name__ == '__main__':

    if args.quantum:
        useQuantum()
    elif args.urandom:
        useSystem()
    elif args.pseudo:
        usePseudo()
    else:
        print("Select a generation method")
        exit()

    genPad(otp)
