# otpGen

One time pad generation script.

# Dependencies:

quantumrandom

`pip install quantumrandom`

# Install:

`pip install PyInstaller`

`pyinstaller --onefile otpgen.py`

`cp dist/otpgen /usr/local/bin`

# Uninstall:

`rm -rf /usr/local/bin/otpgen`

# Notes:

* the -p/--pseudo flag should not be used for any security-critical situations

* the -q/--quantum flag transfers data over the internet via tls/ssl
